﻿// ConsoleApplication6.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

#include "Definitions.h"

using namespace std;

#include "database.h"
#include <windows.h>
#include <fstream>

#include "opencl_driver.h"


float formas_tmp[imagens_por_vez*problem_dimension];
int main()
{
	initGpuAlgorithm();

	vector<vector<float>> triangle_descriptors = getAllTriangleDescriptors("mpeg.txt");

	vector<float> resultados;

	LARGE_INTEGER t1;
	QueryPerformanceCounter(&t1);

	vector<pair<int, int>> comparacoes_a_fazer;
	comparacoes_a_fazer.reserve(triangle_descriptors.size());

	for (int a = 0; a < triangle_descriptors.size(); a++)
	{
		for (int i = a; i < triangle_descriptors.size(); i++)
		{
			comparacoes_a_fazer.push_back(std::pair<int, int>( a, i));
		}
		//cout << "processando imagem " << a << " de " << database.size() << endl;
	}
	
	for (int i = 0; i < comparacoes_a_fazer.size(); )
	{
		int a = 0;

		for (; a < comparacoes_por_vez; a++)
		{
			vector<float>& f1 = triangle_descriptors[comparacoes_a_fazer[i + a].first];
			memcpy(formas_tmp + (2 * a)*problem_dimension, &f1[0], sizeof(float)*problem_dimension);
			vector<float>& f2 = triangle_descriptors[comparacoes_a_fazer[i + a].second];
			memcpy(formas_tmp + (2 * a + 1)*problem_dimension, &f2[0], sizeof(float)*problem_dimension);
			
			f1.erase(f1.end() - 1);
			f2.erase(f2.end() - 1);

			/*cout << endl;
			for (int i = 0; i < problem_dimension; i++)
				cout << dados_formas[problem_dimension+i] << " ";

			cout << endl;*/
		}

		float* resp = calcularScores(formas_tmp);

		for (int i = 0; i < comparacoes_por_vez; i++)
			resultados.push_back(resp[i]);

		cout << "processando lote de " << i << " a " << (i + comparacoes_por_vez -1) << " (no total " << comparacoes_a_fazer.size() << ")" << endl;
		i += a;

	}



	LARGE_INTEGER t2;
	QueryPerformanceCounter(&t2);
	LARGE_INTEGER freq;
	QueryPerformanceFrequency(&freq);
	std::cout << "tempo: " << (t2.QuadPart - t1.QuadPart) / (float)(freq.QuadPart) << std::endl;

	fstream resultados_stream("resultados.txt", fstream::out);

	for (float a : resultados)
		resultados_stream << a << endl;

	cleanGpu();

	system("pause");
	return 0;
}