#pragma once
#include <CL\cl.hpp>

extern cl::Program __programa;
extern cl::Context __context;
extern cl::Device __default_device;
extern cl::CommandQueue queue;
const char *getErrorString(int error);

void initOpencl();
